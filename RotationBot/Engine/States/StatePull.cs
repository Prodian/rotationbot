﻿using RotationBot.Interfaces;
using ZzukBot.Constants;
using RotationBot.Settings;
using ZzukBot.ExtensionFramework;
using ZzukBot.Game.Statics;
using ZzukBot.Mem;
using ZzukBot.Objects;

namespace RotationBot.Engine.States
{
    class StatePull : IState
    {
        public int Priority => 95;

        //We want to run if autopull is 'true' in the settings and we have a new target
        public bool NeedToRun => RotationBotSettings.Values.AutoPull &&
                                 ObjectManager.Instance.Target != null &&
                                 !ObjectManager.Instance.Target.IsInCombat;

        public void Run()
        {
            //Save the current target
            WoWUnit targetUnit = ObjectManager.Instance.Target;

            if (targetUnit == null)
                return;

            //Check the target isn't friendly or dead
            if (targetUnit.Reaction == Enums.UnitReaction.Friendly ||
                targetUnit.Health <= 1)
                return; //Do Nothing
                

            //Check if the target is a player and we don't want to fight players
            if (targetUnit.IsPlayer && !RotationBotSettings.Values.FightPlayers)
                return; //Do nothing!

            //Los Check
            if (RotationBotSettings.Values.LosCheck && !ObjectManager.Instance.Player.InLosWith(targetUnit))
                return;

            CustomClasses.Instance.Current.OnPull(); //Fight!
        }
    }
}
