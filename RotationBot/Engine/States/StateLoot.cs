﻿using System;
using System.Collections.Generic;
using RotationBot.Interfaces;
using ZzukBot.Objects;
using System.Linq;
using RotationBot.Settings;
using ZzukBot.Game.Statics;
using ZzukBot.Mem;

namespace RotationBot.Engine.States
{
    class StateLoot : IState
    {
        //Priority in the FSM (Highest becasue we want to check for loot before anything else)
        public int Priority => 100;

        Dictionary<ulong, int> lootBlacklist = new Dictionary<ulong, int>(); 
        //Run this state if 
        //The setting for autoloot is set to 'true'
        //and the loot window is open 
        //OR there is a mob close to us which can be looted
        public bool NeedToRun => RotationBotSettings.Values.AutoLoot && 
                                 !ObjectManager.Instance.Player.IsInCombat &&                   
                                 (ZzukBot.Game.Frames.LootFrame.IsOpen || (GetLootableMob() != null));

        public void Run()
        {

            //If the loot frame is open then we want to loot all the items!
            if (ZzukBot.Game.Frames.LootFrame.IsOpen)
            {
                ZzukBot.Game.Frames.LootFrame.Instance.LootAll();                
            }


            //Try to get a mob to loot
            WoWUnit lootUnit = GetLootableMob();

            //If the mob exists
            if (lootUnit != null)
            {
                //Interact with it
                if(ZzukBot.Helpers.Wait.For("LootInteractWait", RotationBotSettings.Values.AutoLootDelay))
                    lootUnit.Interact(false);

                lootBlacklist.Add(lootUnit.Guid, Environment.TickCount + 20000);
            }



        }

        //The distance to loot a mob
        private readonly int _lootDistance = 7;

        //Execute in mainthread
        private WoWUnit GetLootableMob()
        {
            return (WoWUnit) MainThread.Instance.Invoke(() => 
                UnitInfo.Instance.Lootable.FirstOrDefault(i => i.DistanceToPlayer <= _lootDistance && !IsLootBlackisted(i.Guid)) ?? null);
        }

        private bool IsLootBlackisted(ulong guid)
        {
            int time = 0;
            if (!lootBlacklist.TryGetValue(guid, out time))
                return false;
            if (time < Environment.TickCount)
            {
                lootBlacklist.Remove(guid);
                return false;
            }
            return true;
        }
    }
}
