﻿using System;
using RotationBot.Interfaces;
using RotationBot.Settings;
using ZzukBot.ExtensionFramework;
using ZzukBot.Objects;
using ZzukBot.Game.Statics;
using ZzukBot.Mem;

namespace RotationBot.Engine.States
{
    class StateFight : IState
    {

        public StateFight()
        {

        }

        public int Priority => 90;

        public bool NeedToRun => ObjectManager.Instance.Player.IsInCombat ||
                                 (ObjectManager.Instance.Target != null &&
                                 ObjectManager.Instance.Target.IsInCombat);

        public void Run()
        {
            //Save the current target
            WoWUnit targetUnit = ObjectManager.Instance.Target;

            //Do a nullcheck
            if (targetUnit == null)
                return;


            //Check that we aren't about to attack someone we shouldn't!
            if (!RotationBotSettings.Values.FightPlayers && targetUnit.IsPlayer || targetUnit.Health <= 0)
                return;

            //Los Check
            if (RotationBotSettings.Values.LosCheck && !ObjectManager.Instance.Player.InLosWith(targetUnit))
                return;

            //Check combat distance
            if (targetUnit.DistanceToPlayer <= CustomClasses.Instance.Current.CombatDistance)
            {
                CustomClasses.Instance.Current.OnFight();
            }
        }
    }
}
