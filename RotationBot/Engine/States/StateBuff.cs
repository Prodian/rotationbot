﻿using RotationBot.Interfaces;
using RotationBot.Settings;
using ZzukBot.ExtensionFramework;
using ZzukBot.Mem;
using ZzukBot.Game.Statics;

namespace RotationBot.Engine.States
{
    class StateBuff : IState
    {
        public int Priority => 80;

        //Messy :(
        //Should be self-explanitory
        public bool NeedToRun =>
            RotationBotSettings.Values.Buff &&
            !ObjectManager.Instance.Player.IsInCombat &&
            !ObjectManager.Instance.Player.IsDead &&
            !ObjectManager.Instance.Player.IsDrinking &&
            !ObjectManager.Instance.Player.IsEating &&
            (ObjectManager.Instance.Player.TargetGuid == 0 ||
             ObjectManager.Instance.Player.TargetGuid ==
             ObjectManager.Instance.Player.Guid);

        public void Run()
        {
            //Work-around for Auto-Attack bug (Probably server-side)
            Spell.Instance.StopAttack();

            //Could force self-targeting logic here
            //But this should be delt with in the CC
            CustomClasses.Instance.Current.OnBuff();
        }
    }
}
