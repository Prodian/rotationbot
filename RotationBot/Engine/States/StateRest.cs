﻿using System.ComponentModel;
using RotationBot.Interfaces;
using RotationBot.Settings;
using ZzukBot.ExtensionFramework;
using ZzukBot.Mem;
using ZzukBot.Game.Statics;

namespace RotationBot.Engine.States
{
    //Rest State
    class StateRest : IState
    {
        public int Priority => 85;

        //Messy :(
        //Execute in mainThread
        public bool NeedToRun =>
            (bool) MainThread.Instance.Invoke(() => RotationBotSettings.Values.Rest &&                  //Check rest is enabled
                                                    (RotationBotSettings.Values.HealthRestPercent >     //Check the Heal percent is lower than settings
                                                    ObjectManager.Instance.Player.HealthPercent || 
                                                    (RotationBotSettings.Values.ManaRestPercent >       //Or mana percent is lower
                                                    ObjectManager.Instance.Player.ManaPercent &&        
                                                    ObjectManager.Instance.Player.ManaPercent > 0))&&   //Only if we have 1% mana or more (energy classes issue)
                                                    !ObjectManager.Instance.Player.IsInCombat &&
                                                    !ObjectManager.Instance.Player.IsDead &&
                                                    !ObjectManager.Instance.Player.IsDrinking &&
                                                    !ObjectManager.Instance.Player.IsEating);
        public void Run()
        {
            //Deal with food in the CustomClass
            CustomClasses.Instance.Current.OnRest();
        }

    }
}
