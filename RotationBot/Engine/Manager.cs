﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;
using RotationBot.Interfaces;
using RotationBot.Engine.States;
using RotationBot.Gui;
using RotationBot.Settings;
using ZzukBot.Constants;
using ZzukBot.ExtensionFramework;
using ZzukBot.Game.Statics;
using ZzukBot.Mem;


namespace RotationBot.Engine
{
    public class Manager
    {

        public void LogToFile(string parFile, string parContent)
        {
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "..\\" + parFile;
            File.AppendAllText(path, Environment.TickCount + @": " + parContent + "\n" + @"###" + "\n" + "\n");
        }

        public void PrintToChat(string parMessage)
        {
            Lua.Instance.Execute("DEFAULT_CHAT_FRAME:AddMessage('" + parMessage + "')");
        }

        private static Lazy<Manager> _instance = new Lazy<Manager>(() => new Manager());
        public static Manager Instance => _instance.Value;
        private Manager()
        {
        }

        private List<IState> _states = new List<IState>();
        private bool _runBot = false;
        private Action _callback;
        private Form _selectorForm;

        public bool Start(Action parCallback)
        {
            //Don't run if we are out of game
            if (!ObjectManager.Instance.IsIngame)
                return _runBot = false;

            //Don't run if we are already running
            if (_runBot) return false;
            _runBot = true;
            _callback = parCallback;

            //Load our settings from file
            RotationBotSettings.Values.Load();

            //Load all of the custom classes
            CustomClasses.Instance.Load();

            //Choose a custom class
            if (!ChooseCustomClassByWowClass(ObjectManager.Instance.Player.Class))
            {
                MessageBox.Show(@"Unable to find a CC which we can use.");
                return _runBot = false;
            }

            //Call load on our chosen CC
            if (!CustomClasses.Instance.Current.Load())
            {
                //If the CC cannot load correctly we return alert the user
                MessageBox.Show(CustomClasses.Instance.Current.Name+@" - could not be loaded.");
                return _runBot = false;
            }

            //Add the FSM states
            _states.Clear();
            _states.Add(new StateLoot());
            _states.Add(new StatePull());
            _states.Add(new StateFight());
            _states.Add(new StateBuff());
            _states.Add(new StateRest());
            _states = _states.OrderByDescending(i => i.Priority).ToList();

            //Start the bot
            Task.Run(() => { Pulse(); });
            return true;
        }

        public void Stop()
        {
            //Stop the bot
            _runBot = false;
            //Clear the FSM
            _states.Clear();
        }

        //Pulse logic
        private void Pulse()
        {
            int spellBookUpdate = Environment.TickCount;
            while (_runBot && ObjectManager.Instance.IsIngame) //If we are enabled
            {
                foreach (var li in _states) //For each state
                {
                    //TryCatch blocks are not ideal - but useful for debugging
                    try
                    {
                        if ((spellBookUpdate + 30000) < Environment.TickCount)
                        {
                            spellBookUpdate = Environment.TickCount;
                            Spell.UpdateSpellbook();
                        }
                        //If we are mounted don't run!
                        if (ObjectManager.Instance.Player.IsMounted)
                            break;
                        //If state needs to run break -> start from highest priority again
                        if (!li.NeedToRun) continue;

                        //If we want to know what was triggered
                        if(RotationBotSettings.Values.PrintStateToChat)
                            PrintToChat("Running State: "+li);
                        
                        //Run the state then break (and start over!)
                        li.Run();
                        break;
                    }
                    catch (Exception e)
                    {
                        //I want to know if there are bugs!
                        PrintToChat("Exception caught in state " + li + " and recorded.");
                        LogToFile("Exception", "State -> " + li + ". -> "+ e.Message + " \n\n " + e.StackTrace);
                    }
                }
                //Prevent afk
                ObjectManager.Instance.Player.AntiAfk();
                Thread.Sleep(100);
                
            }
            _callback();
        }

        //Function to select custom class
        internal bool ChooseCustomClassByWowClass(Enums.ClassId wowClass)
        {
            //Collect a list of possible CCs to use
            List<int> possibleClasses = new List<int>();
            for (var i = 0; i < CustomClasses.Instance.Enumerator.Count; i++)
            {
                //If we find one for our class
                if (CustomClasses.Instance.Enumerator.ElementAt(i).Class == wowClass)
                {
                    //Add it to the list of possibles
                    possibleClasses.Add(i);
                    //Set it as the current CC to use
                    CustomClasses.Instance.SetCurrent(i);
                }
            }
            //If we had no CCs to use return false
            if (possibleClasses.Count != 0)
            {
                //If we have more than 1, let the user decide which one to use
                if (possibleClasses.Count > 1)
                {
                    //Load the form
                    _selectorForm = new CustomClassSelector(possibleClasses);
                    //Class to use is set in the GUI (nothing to return)
                    _selectorForm.ShowDialog();
                    _selectorForm.Close();
                    _selectorForm.Dispose();
                }
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}