﻿using System;

namespace RotationBot.Settings
{
    public class RotationBotSettings
    {
        public volatile bool PrintStateToChat = false;
        public volatile bool AutoLoot = true;
        public volatile int AutoLootDelay = 750;
        public volatile bool AutoPull= true;
        public volatile bool Buff = true;
        public volatile bool Rest = false;
        public volatile bool FightPlayers = false;
        public volatile int ManaRestPercent = 40;
        public volatile int HealthRestPercent = 40;
        public volatile bool LosCheck = true;

        private RotationBotSettings()
        {
            
        }

        internal static RotationBotSettings Values { get; set; } = new RotationBotSettings();

        internal void Load()
        {
            //If the .json file doesn't exist use default values.
            Values = ZzukBot.Settings.OptionManager.Get("RotationBotSettings").LoadFromJson<RotationBotSettings>() ??
                     this;
        }

        internal void Save()
        {
            ZzukBot.Settings.OptionManager.Get("RotationBotSettings").SaveToJson(this);
        }

    }

}
