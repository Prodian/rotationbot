﻿namespace RotationBot.Interfaces
{
    //Object for the FSM states to use
    public interface IState
    {
        int Priority { get; }
        bool NeedToRun { get; }
        void Run();
    }
}
