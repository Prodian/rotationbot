﻿namespace RotationBot.Gui
{
    partial class CustomClassSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ccLst = new System.Windows.Forms.ListBox();
            this.selectBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ccLst
            // 
            this.ccLst.FormattingEnabled = true;
            this.ccLst.Location = new System.Drawing.Point(12, 12);
            this.ccLst.Name = "ccLst";
            this.ccLst.Size = new System.Drawing.Size(155, 160);
            this.ccLst.TabIndex = 0;
            // 
            // selectBtn
            // 
            this.selectBtn.Location = new System.Drawing.Point(12, 177);
            this.selectBtn.Name = "selectBtn";
            this.selectBtn.Size = new System.Drawing.Size(155, 28);
            this.selectBtn.TabIndex = 1;
            this.selectBtn.Text = "Select";
            this.selectBtn.UseVisualStyleBackColor = true;
            this.selectBtn.Click += new System.EventHandler(this.selectBtn_Click);
            // 
            // CustomClassSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(179, 217);
            this.Controls.Add(this.selectBtn);
            this.Controls.Add(this.ccLst);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximumSize = new System.Drawing.Size(195, 256);
            this.MinimumSize = new System.Drawing.Size(195, 256);
            this.Name = "CustomClassSelector";
            this.Text = "CustomClassSelector";
            this.Load += new System.EventHandler(this.CustomClassSelector_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox ccLst;
        private System.Windows.Forms.Button selectBtn;
    }
}