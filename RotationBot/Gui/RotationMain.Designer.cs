﻿namespace RotationBot.Gui
{
    partial class RotationMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RotationMain));
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.restCb = new System.Windows.Forms.CheckBox();
            this.restManaNu = new System.Windows.Forms.NumericUpDown();
            this.buffCb = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.restHealthNu = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.losCb = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lootDelayNu = new System.Windows.Forms.NumericUpDown();
            this.lootCb = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pullCb = new System.Windows.Forms.CheckBox();
            this.attackPlayersCb = new System.Windows.Forms.CheckBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.printStateCb = new System.Windows.Forms.CheckBox();
            this.ccGUI = new System.Windows.Forms.Button();
            this.saveBtn = new System.Windows.Forms.Button();
            this.reloadBtn = new System.Windows.Forms.Button();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.restManaNu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.restHealthNu)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lootDelayNu)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.restCb);
            this.groupBox3.Controls.Add(this.restManaNu);
            this.groupBox3.Controls.Add(this.buffCb);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.restHealthNu);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Location = new System.Drawing.Point(6, 84);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(173, 121);
            this.groupBox3.TabIndex = 18;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Rest / Buff";
            // 
            // restCb
            // 
            this.restCb.AutoSize = true;
            this.restCb.Location = new System.Drawing.Point(6, 42);
            this.restCb.Name = "restCb";
            this.restCb.Size = new System.Drawing.Size(48, 17);
            this.restCb.TabIndex = 5;
            this.restCb.Text = "Rest";
            this.restCb.UseVisualStyleBackColor = true;
            // 
            // restManaNu
            // 
            this.restManaNu.Location = new System.Drawing.Point(84, 97);
            this.restManaNu.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.restManaNu.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.restManaNu.Name = "restManaNu";
            this.restManaNu.Size = new System.Drawing.Size(84, 20);
            this.restManaNu.TabIndex = 17;
            this.restManaNu.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // buffCb
            // 
            this.buffCb.AutoSize = true;
            this.buffCb.Location = new System.Drawing.Point(6, 19);
            this.buffCb.Name = "buffCb";
            this.buffCb.Size = new System.Drawing.Size(45, 17);
            this.buffCb.TabIndex = 4;
            this.buffCb.Text = "Buff";
            this.buffCb.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Rest Health %";
            // 
            // restHealthNu
            // 
            this.restHealthNu.Location = new System.Drawing.Point(84, 70);
            this.restHealthNu.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.restHealthNu.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.restHealthNu.Name = "restHealthNu";
            this.restHealthNu.Size = new System.Drawing.Size(84, 20);
            this.restHealthNu.TabIndex = 16;
            this.restHealthNu.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Rest Mana %";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.losCb);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.lootDelayNu);
            this.groupBox2.Controls.Add(this.lootCb);
            this.groupBox2.Location = new System.Drawing.Point(185, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(110, 114);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Misc";
            // 
            // losCb
            // 
            this.losCb.AutoSize = true;
            this.losCb.Location = new System.Drawing.Point(6, 19);
            this.losCb.Name = "losCb";
            this.losCb.Size = new System.Drawing.Size(79, 17);
            this.losCb.TabIndex = 3;
            this.losCb.Text = "Check LoS";
            this.losCb.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Delay in ms";
            // 
            // lootDelayNu
            // 
            this.lootDelayNu.Location = new System.Drawing.Point(9, 81);
            this.lootDelayNu.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.lootDelayNu.Name = "lootDelayNu";
            this.lootDelayNu.Size = new System.Drawing.Size(61, 20);
            this.lootDelayNu.TabIndex = 1;
            this.lootDelayNu.Value = new decimal(new int[] {
            750,
            0,
            0,
            0});
            // 
            // lootCb
            // 
            this.lootCb.AutoSize = true;
            this.lootCb.Checked = true;
            this.lootCb.CheckState = System.Windows.Forms.CheckState.Checked;
            this.lootCb.Location = new System.Drawing.Point(6, 42);
            this.lootCb.Name = "lootCb";
            this.lootCb.Size = new System.Drawing.Size(69, 17);
            this.lootCb.TabIndex = 0;
            this.lootCb.Text = "AutoLoot";
            this.lootCb.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pullCb);
            this.groupBox1.Controls.Add(this.attackPlayersCb);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(173, 74);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Combat";
            // 
            // pullCb
            // 
            this.pullCb.AutoSize = true;
            this.pullCb.Checked = true;
            this.pullCb.CheckState = System.Windows.Forms.CheckState.Checked;
            this.pullCb.Location = new System.Drawing.Point(6, 42);
            this.pullCb.Name = "pullCb";
            this.pullCb.Size = new System.Drawing.Size(68, 17);
            this.pullCb.TabIndex = 1;
            this.pullCb.Text = "Auto Pull";
            this.pullCb.UseVisualStyleBackColor = true;
            // 
            // attackPlayersCb
            // 
            this.attackPlayersCb.AutoSize = true;
            this.attackPlayersCb.Checked = true;
            this.attackPlayersCb.CheckState = System.Windows.Forms.CheckState.Checked;
            this.attackPlayersCb.Location = new System.Drawing.Point(6, 19);
            this.attackPlayersCb.Name = "attackPlayersCb";
            this.attackPlayersCb.Size = new System.Drawing.Size(94, 17);
            this.attackPlayersCb.TabIndex = 0;
            this.attackPlayersCb.Text = "Attack Players";
            this.attackPlayersCb.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(2, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(315, 241);
            this.tabControl1.TabIndex = 22;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(307, 215);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Combat";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(307, 215);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Debug";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.printStateCb);
            this.groupBox4.Location = new System.Drawing.Point(6, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(121, 47);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Debug";
            // 
            // printStateCb
            // 
            this.printStateCb.AutoSize = true;
            this.printStateCb.Location = new System.Drawing.Point(6, 19);
            this.printStateCb.Name = "printStateCb";
            this.printStateCb.Size = new System.Drawing.Size(112, 17);
            this.printStateCb.TabIndex = 0;
            this.printStateCb.Text = "Print State to Chat";
            this.printStateCb.UseVisualStyleBackColor = true;
            // 
            // ccGUI
            // 
            this.ccGUI.Location = new System.Drawing.Point(210, 247);
            this.ccGUI.Name = "ccGUI";
            this.ccGUI.Size = new System.Drawing.Size(96, 23);
            this.ccGUI.TabIndex = 25;
            this.ccGUI.Text = "Show CC Gui";
            this.ccGUI.UseVisualStyleBackColor = true;
            this.ccGUI.Click += new System.EventHandler(this.ccGUI_Click);
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(6, 247);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(96, 23);
            this.saveBtn.TabIndex = 23;
            this.saveBtn.Text = "Save";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // reloadBtn
            // 
            this.reloadBtn.Location = new System.Drawing.Point(108, 247);
            this.reloadBtn.Name = "reloadBtn";
            this.reloadBtn.Size = new System.Drawing.Size(96, 23);
            this.reloadBtn.TabIndex = 24;
            this.reloadBtn.Text = "Reload";
            this.reloadBtn.UseVisualStyleBackColor = true;
            this.reloadBtn.Click += new System.EventHandler(this.reloadBtn_Click);
            // 
            // RotationMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 276);
            this.Controls.Add(this.ccGUI);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.reloadBtn);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RotationMain";
            this.Text = "Rotation Bot Settings v4";
            this.Load += new System.EventHandler(this.RotationMain_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.restManaNu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.restHealthNu)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lootDelayNu)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox restCb;
        private System.Windows.Forms.NumericUpDown restManaNu;
        private System.Windows.Forms.CheckBox buffCb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown restHealthNu;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox lootCb;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox pullCb;
        private System.Windows.Forms.CheckBox attackPlayersCb;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox printStateCb;
        private System.Windows.Forms.Button ccGUI;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Button reloadBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown lootDelayNu;
        private System.Windows.Forms.CheckBox losCb;
    }
}