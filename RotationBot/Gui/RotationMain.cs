﻿using System;
using System.Windows.Forms;
using RotationBot.Settings;
using ZzukBot.ExtensionFramework;

namespace RotationBot.Gui
{
    public partial class RotationMain : Form
    {
        public RotationMain()
        {
            InitializeComponent();
        }

        private void RotationMain_Load(object sender, EventArgs e)
        {
            LoadFields();
        }


        private void reloadBtn_Click(object sender, EventArgs e)
        {
            LoadFields();

        }

        //Load from the Json file
        //Change the fields to match the settings
        private void LoadFields()
        {
            //Load
            RotationBotSettings.Values.Load();

            //Combat Tab
            attackPlayersCb.Checked = RotationBotSettings.Values.FightPlayers;
            pullCb.Checked = RotationBotSettings.Values.AutoPull;
            buffCb.Checked = RotationBotSettings.Values.Buff;
            restCb.Checked = RotationBotSettings.Values.Rest;
            lootCb.Checked = RotationBotSettings.Values.AutoLoot;
            lootDelayNu.Value = RotationBotSettings.Values.AutoLootDelay;
            restHealthNu.Value = RotationBotSettings.Values.HealthRestPercent;
            restManaNu.Value = RotationBotSettings.Values.ManaRestPercent;
            losCb.Checked = RotationBotSettings.Values.LosCheck;

            //Dev Tab
            printStateCb.Checked = RotationBotSettings.Values.PrintStateToChat;
        }

        //Change the settings to match the fields
        //Save to the Json file
        private void saveBtn_Click(object sender, EventArgs e)
        {
            //Combat Tab
            RotationBotSettings.Values.FightPlayers = attackPlayersCb.Checked;
            RotationBotSettings.Values.AutoPull = pullCb.Checked;
            RotationBotSettings.Values.AutoLootDelay = (int)lootDelayNu.Value;
            RotationBotSettings.Values.Buff = buffCb.Checked;
            RotationBotSettings.Values.Rest = restCb.Checked;
            RotationBotSettings.Values.AutoLoot = lootCb.Checked;
            RotationBotSettings.Values.HealthRestPercent = (int)restHealthNu.Value;
            RotationBotSettings.Values.ManaRestPercent = (int)restManaNu.Value;
            RotationBotSettings.Values.LosCheck = losCb.Checked;

            //Dev Tab
            RotationBotSettings.Values.PrintStateToChat = printStateCb.Checked;

            //Save
            RotationBotSettings.Values.Save();
        }

        private void ccGUI_Click(object sender, EventArgs e)
        {
            //CC isn't set until the botbase has been started (instance exists)
            if (CustomClasses.Instance.Current != null)
            {
                CustomClasses.Instance.Current.ShowGui();
            }
            else
            {
                MessageBox.Show(@"No CC Set - Please Start the BotBase first.");
            }
            
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }
    }
}
