﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZzukBot.ExtensionFramework;

namespace RotationBot.Gui
{
    public partial class CustomClassSelector : Form
    {
        //Using a dictionary to keep track of CustomClasses position in Enum
        private Dictionary<string, int> ClassDictionary;

        //Constructor will build the dictionary and add the names to the listbox.
        public CustomClassSelector(List<int> ccs)
        {
            InitializeComponent();
            ClassDictionary = new Dictionary<string, int>();
            foreach (var i in ccs)
            {
                ccLst.Items.Add(CustomClasses.Instance.Enumerator.ElementAt(i).Name);
                ClassDictionary.Add(CustomClasses.Instance.Enumerator.ElementAt(i).Name, i);
            }
        }

        //When the select button is clicked
        private void selectBtn_Click(object sender, EventArgs e)
        {
            //Check the user has exactly 1 item selected
            if (!ccLst.SelectedIndex.Equals(null) && ccLst.SelectedItems.Count == 1)
            {
                //Set the selected CC (Using position stored in dictionary)
                CustomClasses.Instance.SetCurrent(ClassDictionary[ccLst.Items[ccLst.SelectedIndex].ToString()]);
                //Force the form to close
                this.Dispose();
            }
        }

        private void CustomClassSelector_Load(object sender, EventArgs e)
        {

        }
    }
}
