﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Forms;
using RotationBot.Engine;

namespace RotationBot
{
    [Export(typeof(ZzukBot.ExtensionFramework.Interfaces.IBotBase))]
    public class RotationBot : ZzukBot.ExtensionFramework.Interfaces.IBotBase
    {
        private string _name = "RotationBot";
        private string _author = "Emu";
        private int _version = 5;

        Form _guiForm = new Gui.RotationMain();
        public bool Start(Action onStopCallback)
        {
            this.ShowGui();
            return Manager.Instance.Start(onStopCallback);
        }

        public void Stop()
        {
            Manager.Instance.Stop();
            _guiForm.Close();
            _guiForm.Dispose();
        }

        public void ShowGui()
        {
            _guiForm.Dispose();
            _guiForm = new Gui.RotationMain();
            _guiForm.Show();
        }

        public void _Dispose()
        {
            this.Stop();
        }

        public string Name
        {
            get
            { return _name; }
        }

        public string Author
        {
            get { return _author; }
        }
        public int Version
        {
            get { return _version; }
        }
    }
}

